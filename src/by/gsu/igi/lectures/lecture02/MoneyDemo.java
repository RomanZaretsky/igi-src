package by.gsu.igi.lectures.lecture02;

import java.math.BigDecimal;

/**
 * Created by Evgeniy Myslovets.
 */
public class MoneyDemo {

    public static void main(String[] args) {
        floatDemo();
        bigDecimalDemo();
    }

    private static void floatDemo() {
        float balance = 1;
        balance -= 0.1;
        balance -= 0.1;
        balance -= 0.1;
        balance -= 0.1;

        System.out.println(balance);
        boolean isEnough = balance >= 0.6;
        System.out.println("Enough for another beer? " + isEnough);
    }

    private static void bigDecimalDemo() {
        BigDecimal balance = new BigDecimal(1);
        balance = balance.subtract(new BigDecimal(0.1));
        balance = balance.subtract(new BigDecimal(0.1));
        balance = balance.subtract(new BigDecimal(0.1));
        balance = balance.subtract(new BigDecimal(0.1));

        System.out.println(balance);
        balance = balance.setScale(2, BigDecimal.ROUND_HALF_UP);
        System.out.println(balance);
        boolean isEnough = balance.compareTo(new BigDecimal(0.6)) > 0;
        System.out.println("Enough for another beer? " + isEnough);
    }
}
