package by.gsu.igi.students.RomanZaretsky;

import java.util.Scanner;

public class RomanHelper {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\\n");
        System.out.print("Введите радиус");
        double radius = scanner.nextDouble();
        double perimeter = Math.PI * 2 * radius;
        double area = Math.PI * radius * radius;
        System.out.println("Периметр равен" + perimeter);
        System.out.print("Площадь равна" + area);

    }

}
