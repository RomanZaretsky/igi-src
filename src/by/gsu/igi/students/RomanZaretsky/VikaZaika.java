package by.gsu.igi.students.RomanZaretsky;

import java.util.Scanner;

public class VikaZaika {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\\n");
        String nextString = scanner.next();
        System.out.println(nextString);
        int nextInt = scanner.nextInt();
        System.out.println(nextInt);
    }
}
